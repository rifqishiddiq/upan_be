<?php

namespace App\Traits;

trait Modifier {
    protected static function bootModifier() {
        static::creating(function ($model) {
            $user = auth()->user();
            if ($user) {
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            }
        });

        static::updating(function ($model) {
            $user = auth()->user();
            if ($user) {
                $model->updated_by = $user->id;
                // $model->updated_at = date('Y-m-d H:i:s');
            }
        });
    }

}
