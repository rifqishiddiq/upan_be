<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Traits\HttpResponses;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\ItemImage;

class ItemController extends Controller
{
    use HttpResponses;
    //
    public function index()
    {
        return $this->success(Item::all());
    }

    public function showItem(Request $request, $id)
    {
        $data = Item::with('item_image')->find($id);
        return $this->success($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' =>'required|max:255',
            'name' =>'required|max:255',
            'qty' =>'required|numeric|min:0',
            'qty_available' =>'required|numeric|min:0',
            'qty_borrowed' =>'required|numeric|min:0',
        ]);

        $request->created_by = auth()->user()->id;

        $data = Item::create($request->all());
        return $this->success($data);
    }

    public function update(Request $request, $id)
    {
        $request->user_id = array_map('intval', explode(';', $request->user_id));

        $this->validate($request, [
            'code' =>'required|max:255',
            'name' =>'required|max:255',
            'qty' =>'required|numeric|min:0',
            'qty_available' =>'required|numeric|min:0',
            'qty_borrowed' =>'required|numeric|min:0',
        ]);

        $data = Item::find($id);

        $data->code = $request->code;
        $data->name = $request->name;
        $data->desc = $request->desc;
        $data->qty = $request->qty;
        $data->qty_available = $request->qty_available;
        $data->qty_borrowed = $request->qty_borrowed;
        $data->update();

        // $arr = [];
        // $arr_id_delete = [];

        // foreach ($request->user_id as $key => $value) {
        //     $data_temp = [
        //         'user_id' => $value,
        //         'event_id' => $data->id,
        //         'created_at' => date('Y-m-d H:i:s'),
        //         'updated_at' => date('Y-m-d H:i:s')
        //     ];
        //     $found = EventMember::where([
        //         ['event_id', '=', $id],
        //         ['user_id', '=', $value],
        //     ])->get();
        //     if (count($found) === 0) {
        //         array_push($arr, $data_temp);
        //     }
        // }
        // $notFound = EventMember::where('event_id', $id)->whereNotIn('user_id', $request->user_id)->delete();
        // $data_sub = EventMember::insert($arr);

        return $this->success($data);
    }

    public function delete($id)
    {
        $data = Item::find($id); 
        // if ($data === null) {
        //     return $this->error('', 'ID not found!', 400);
        // }

        $item_id = $data->id;
        $deleteItemImage = ItemImage::where('item_id', $item_id)->delete();
        $data->delete();
        return $this->success('', "data deleted successfully!");
    }

}
