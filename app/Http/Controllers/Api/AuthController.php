<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserRole;
use App\Models\UserRoleMapping;

class AuthController extends Controller
{
    use HttpResponses;

    public function login(Request $request)
    {
        // Validation
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $this->validate($request, [
            'role_id' =>'required|exists:user_roles,id',
        ]);

        // Check User
        if (!auth()->attempt($credentials)) {
            return $this->error('', 'Your credential does not match!', 401);
        }
        $user = auth()->user();

        // Check Role
        $roleMapping = UserRoleMapping::where('user_id', $user->id)->where('role_id', $request->role_id)->first();
        if ($roleMapping === null) {
            return $this->error([$user->id, $request->role_id], 'User does not have the selected role!', 401);
        }
        $role = UserRole::find($roleMapping->role_id);
        $user->role_id_active = $role->id;

        // Get Menu
        $menu = DB::table('menus as m')->select(
            'm.id',
            'm.code',
            'm.category',
            'm.name',
        )
        ->leftjoin('menu_role_mappings as b', 'b.menu_id', '=', 'm.id')
        ->where('b.role_id', $request->role_id)->get();

        User::where('id', $user->id)->update(['role_id_active' => $role->id]);

        return $this->success([
            'user' => $user,
            'role' => $role,
            'menu' => $menu,
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
        
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' =>'required|max:255',
            'phone' =>'required|max:15|unique:users',
            'email' =>'required|email|max:255|unique:users',
            'password' =>'required_with:password_confirmation|min:8|confirmed',
            'role_id' =>'required|exists:user_roles,id',
        ]);

        $user = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        UserRoleMapping::create([
            'user_id' => $user->id,
            'role_id' => $request->role_id,
        ]);

        return $this->success([
            'user' => $user,
            // 'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        User::where('id', auth()->user()->id)->update(['role_id_active' => null]);
        auth()->user()->currentAccessToken()->delete();

        return $this->success([
            'message' => 'User logged out Successfully!',
        ]);
    }

    public function checkIfAuthenticated()
    {
        if (auth()->check()) {
            $user = auth()->user();
            // Check Role
            $roleMapping = UserRoleMapping::where('user_id', $user->id)->where('role_id', $user->role_id_active)->first();
            if ($roleMapping === null) {
                return $this->error('', 'User does not have the selected role!', 401);
            }
            $role = UserRole::find($roleMapping->role_id);

            // Get Menu
            $menu = DB::table('menus as m')->select(
                'm.id',
                'm.code',
                'm.category',
                'm.name',
            )
            ->leftjoin('menu_role_mappings as b', 'b.menu_id', '=', 'm.id')
            ->where('b.role_id', $user->role_id_active)->get();

            return $this->success([
                'user' => $user,
                'role' => $role,
                'menu' => $menu,
                'message' => 'You are Authenticated!',
            ]);
        } else {
            return $this->error('', 'You are not Authenticated', 401);
        }
    }
}
