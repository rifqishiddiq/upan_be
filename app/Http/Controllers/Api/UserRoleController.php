<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Models\UserRole;

class UserRoleController extends Controller
{
    use HttpResponses;
    //
    public function index()
    {
        return $this->success(UserRole::all());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' =>'required|max:255',
            'name' =>'required|max:255',
        ]);

        $data = UserRole::create($request->all());
        return $this->success($data);
    }

}
