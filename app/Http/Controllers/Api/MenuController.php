<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use App\Models\Menu;
use App\Models\MenuRoleMapping;

class MenuController extends Controller
{
    use HttpResponses;

    public function index()
    {
        return $this->success(Menu::all());
    }

    public function getMenuByRole(Request $request)
    {
        // $data = Menu::leftjoin('menu_role_mappings as b', 'b.menu_id', '=', 'menus.id')->where('b.role_id', $request->role_id)->get();
        $data = DB::table('menus as m')->select(
            'm.id',
            'm.code',
            'm.category',
            'm.name',
        )
        ->leftjoin('menu_role_mappings as b', 'b.menu_id', '=', 'm.id')
        ->where('b.role_id', $request->role_id)->get();

        return $this->success($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' =>'required|max:255',
            'category' =>'required|max:255',
            'name' =>'required|max:255',
        ]);

        $data = Menu::create($request->all());
        return $this->success($data);
    }

    public function assignMenuByRole(Request $request)
    {
        $request->role_id = array_map('intval', explode(';', $request->role_id));

        $this->validate($request, [
            'role_id' =>'required|exists:user_roles,id',
            'menu_id' =>'required|exists:menus,id',
        ]);

        $arr = [];
        foreach ($request->role_id as $key => $value) {
            $data_temp = [
                'role_id' => $value,
                'menu_id' => $request->menu_id,
                'created_by' => auth()->user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            array_push($arr, $data_temp);
        }

        $delete = MenuRoleMapping::where('menu_id', $request->menu_id)->whereNotIn('role_id', $request->role_id)->delete();
        $data = MenuRoleMapping::insert($arr);
        return $this->success($data);
    }

}
