<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\MenuRoleMapping;

class Menu extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'code',
        'category',
        'name',
    ];

    public function menu_role_mapping()
    {
        return $this->hasMany(MenuRoleMapping::class);
    }
}
