<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\Item;

class ItemImage extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'item_id',
        'src',
        'mime_type',
        'title',
        'alt',
        'description',
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
