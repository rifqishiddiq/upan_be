<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\UserRoleMapping;

class UserRole extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'code',
        'name',
    ];

    public function user_role_mapping()
    {
        return $this->hasMany(UserRoleMapping::class);
    }
}
