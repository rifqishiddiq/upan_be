<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\User;
use App\Models\Menu;

class MenuRoleMapping extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'user_id',
        'menu_id',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
