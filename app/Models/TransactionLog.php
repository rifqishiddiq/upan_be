<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\Item;
use App\Models\User;

class TransactionLog extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'item_id',
        'user_id',
        'note',
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
