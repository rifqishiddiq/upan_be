<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\ItemImage;

class Item extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'code',
        'name',
        'desc',
        'qty',
        'qty_avaiable',
        'qty_borrowed',
    ];

    public function item_image()
    {
        return $this->hasMany(ItemImage::class);
    }
}
