<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;
use App\Models\Item;
use App\Models\User;
use App\Models\TransactionLog;

class Transaction extends Model
{
    use HasFactory;
    use Modifier;

    protected $fillable = [
        'item_id',
        'student_id',
        'lecturer_id',
        'laboran_id',
        'qty',
        'qty_return',
        'borrow_date',
        'return_date',
        'deadline_date',
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function student()
    {
        return $this->belongsTo(User::class);
    }

    public function laboran()
    {
        return $this->belongsTo(User::class);
    }

    public function lectorer()
    {
        return $this->belongsTo(User::class);
    }

    public function transaction_log()
    {
        return $this->hasMany(TransactionLog::class);
    }
}
