<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->unsignedBigInteger('student_id')->nullable(true);
            $table->foreign('student_id')->references('id')->on('users');
            $table->unsignedBigInteger('laboran_id')->nullable(true);
            $table->foreign('laboran_id')->references('id')->on('users');
            $table->unsignedBigInteger('lecturer_id')->nullable(true);
            $table->foreign('lecturer_id')->references('id')->on('users');
            $table->integer('qty')->default(0);
            $table->integer('qty_return')->default(0);
            $table->integer('borrow_date')->timestamps();
            $table->integer('return_date')->timestamps()->nullable(true);
            $table->integer('deadline_date')->timestamps()->nullable(true);
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('status_borrows');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
