<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserRoleController;
use App\Http\Controllers\Api\MenuController;
use App\Http\Controllers\Api\ItemController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Public routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

//Protected routes
Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/verify-auth', [AuthController::class, 'checkIfAuthenticated']);

    Route::group(['prefix' => 'menu'], function (){
        Route::resource('/', MenuController::class);
        Route::get('by-role', [MenuController::class, 'getMenuByRole']);
    });

    Route::group(['prefix' => 'item'], function (){
        Route::resource('/', ItemController::class);
        Route::put('update/{id}', [ItemController::class, 'update']);
        Route::get('show/{id}', [ItemController::class, 'showItem']);
        Route::delete('{id}', [ItemController::class, 'delete']);
    });

});
